import React, { Component } from 'react'
import { ServerStyleSheet } from 'styled-components'
/*
* For Less Support
* */
import autoprefixer from 'autoprefixer'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import postcssFlexbugsFixes from 'postcss-flexbugs-fixes'

const path = require('path')
const fs = require('fs')

const lessToJs = require('less-vars-to-js')

const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, 'src/theme-ant-overwrite.less'), 'utf8'))

const webpack = require('webpack')

//
export default {
  getSiteData: () => ({
    title: 'React Static'
  }),
  getRoutes: async () => [
    {
      path: '/',
      exact: true,
      component: 'src/pages/ViewMain'
    },
    {
      path: '/profile/view',
      exact: true,
      component: 'src/pages/ViewProfile'
    },
    {
      path: '/plan',
      exact: true,
      component: 'src/pages/ViewMap'
    },
    {
      path: '/about',
      exact: true,
      component: 'src/pages/ViewAbout'
    },
    {
      is404: true,
      component: 'src/pages/404'
    }
  ],
  renderToHtml: (render, Comp, meta) => {
    const sheet = new ServerStyleSheet()
    const html = render(sheet.collectStyles(<Comp />))
    meta.styleTags = sheet.getStyleElement()
    return html
  },
  Document: class CustomHtml extends Component {
    render() {
      const { Html, Head, Body, children, renderMeta } = this.props
      return (
        <Html>
          <Head>
            <meta charSet="UTF-8" />
            <meta
              name="description"
              content="Shooting Unicorns is a team of Software Engineers aspiring to build great software products from beautiful Melbourne, Australia. Our bread and butter is React and Node JS."
            />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link href="https://api.tiles.mapbox.com/mapbox-gl-js/v0.48.0/mapbox-gl.css" rel="stylesheet" />
            {renderMeta.styleTags}
          </Head>
          <Body>
            {children}
            <script src="https://api.mapbox.com/mapbox-assembly/mbx/v0.18.0/assembly.js" />
          </Body>
        </Html>
      )
    }
  },
  webpack: (config, { stage, defaultLoaders }) => {
    const jsLoader = {
      test: /\.(js|jsx|ts|tsx)$/,
      exclude: defaultLoaders.jsLoader.exclude, // as std jsLoader exclude
      use: [
        {
          loader: 'babel-loader'
        }
      ]
    }

    /*
    * Less Support
    * */

    // Add .less & .css to resolver
    config.resolve.extensions.push('.less')
    config.resolve.extensions.push('.css')

    // Loader depending on stage. Same format as the default cssLoader.
    let loaders = [
      {
        loader: 'css-loader',
        options: {
          importLoaders: 1,
          minimize: stage !== 'dev',
          sourceMap: true
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          // Necessary for external CSS imports to work
          // https://github.com/facebookincubator/create-react-app/issues/2677
          sourceMap: true,
          ident: 'postcss',
          plugins: () => [
            postcssFlexbugsFixes,
            autoprefixer({
              browsers: [
                '>1%',
                'last 4 versions',
                'Firefox ESR',
                'not ie < 9' // React doesn't support IE8 anyway
              ],
              flexbox: 'no-2009'
            })
          ]
        }
      },
      {
        loader: 'less-loader',
        options: {
          sourceMap: true,
          modifyVars: themeVariables,
          javascriptEnabled: true
        }
      }
    ]

    if (stage === 'dev') {
      // Enable Hot Module Replacement
      config.plugins.push(new webpack.HotModuleReplacementPlugin())

      // In-Line with style-loader
      loaders = ['style-loader'].concat(loaders)
    } else if (stage === 'prod') {
      // Extract to style.css
      loaders = ExtractTextPlugin.extract({
        fallback: {
          loader: 'style-loader',
          options: {
            hmr: false,
            sourceMap: false
          }
        },
        use: loaders
      })
    }

    const lessLoader = {
      test: /\.less$/,
      use: loaders
    }

    /*
    * Add new Loaders to default Loaders
    * */

    config.module.rules = [
      {
        oneOf: [jsLoader, lessLoader, defaultLoaders.cssLoader, defaultLoaders.fileLoader]
      }
    ]

    config.plugins.push(new ExtractTextPlugin('styles.css'))
    if (stage === 'prod') {
      // Update ExtractTextPlugin with current instance
      config.plugins.forEach((plugin, index) => {
        if (plugin instanceof ExtractTextPlugin) {
          config.plugins[index] = new ExtractTextPlugin({
            filename: getPath => {
              process.env.extractedCSSpath = 'styles.css'
              return getPath('styles.css')
            },
            allChunks: true
          })
        }
      })
    }
    return config
  }
}
