# This app was created by Shooting Unicorns 🦄
### https://shooting-unicorns.com
* `npm install`
* `npm start`

# Challenges attempted for GovHack 2018
- Innovation space - A City Planning for Growth
- Activate Melbourne - A Prosperous City
- Bounty: Decision Support

# Dataset used
1. [On-street Parking Bays](https://data.melbourne.vic.gov.au/Transport-Movement/On-street-Parking-Bays/crvt-b4kt)
Description of Use: The GeoJson of On-street Parking Bays was used to visualise parking bays that are under utilised for the opportunity for buskers and creatives to use.


1. [On-street Car Parking Sensor Data - 2017](https://data.melbourne.vic.gov.au/Transport-Movement/On-street-Car-Parking-Sensor-Data-2017/u9sa-j86i)
Description of Use: On-street Car Parking Sensor Data - 2017 was used in conjunction with On-street parking bay sensors and on street parking bays to calculate the aggregated daily usage for determining on-peak and off-peak hours. The idea is to re-purpose under utilised parking bays for buskers and eventually creatives to perform at.


1. [Pedestrian volume (updated monthly)](https://data.melbourne.vic.gov.au/Transport-Movement/Pedestrian-volume-updated-monthly-/b2ak-trbp)
Description of Use: Pedestrian volume (updated monthly) was used in conjunction with pedestrian sensor locations to calculate areas with high foot traffic to allow buskers to determine the best location and time to perform to increase audience reach.


1. [Pedestrian sensor locations](https://data.melbourne.vic.gov.au/Transport-Movement/Pedestrian-sensor-locations/ygaw-6rzq)
Description of Use: Pedestrian sensor locations was used in conjunction with pedestrian volume (updated monthly) to calculate landmark areas with high foot traffic to allow buskers to determine the best location and time to perform to increase audience reach.


1. [On-street Parking Bay Sensors](https://data.melbourne.vic.gov.au/Transport-Movement/On-street-Parking-Bay-Sensors/vh2v-4nfs)
Description of Use: On-street parking bay sensors was used in conjunction with on street parking sensor data 2017 and on street parking bays to calculate the aggregated daily usage of on-peak and off-peak hours. The idea is to re-purpose under utilised parking bays for buskers and eventually creatives to perform at.


# Team
Luannie Dang - Backend Engineer
Samantha Wong - Frontend Engineer