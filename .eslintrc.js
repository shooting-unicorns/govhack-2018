module.exports = {
  "extends": [
    "@medipass/react-medipass",
  ],
  rules: {
    "comma-dangle": ["error", "never"],
    "jsx-a11y/href-no-hash": "off",
    'strict': 0
  },
  parser: 'babel-eslint'
}

