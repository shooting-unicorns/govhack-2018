const fs = require('fs')
const find = require('lodash/fp/find')
const _flattenDeep = require('lodash/flattenDeep')
const isEmpty = require('lodash/isTypedArray')
const pedestrianCount = require('./pedestrian-count.json')

export const mapPedstrianData = () => {
  const geoJsonFeatures = pedestrianCount.map(record => {
    const { longitude, latitude, ...properties } = record
    return {
      type: 'Feature',
      properties: {
        ...properties
      },
      geometry: {
        type: 'Point',
        coordinates: [longitude, latitude]
      }
    }
  })

  const pedestrianGeoJson = {
    type: 'FeatureCollection',
    features: geoJsonFeatures
  }

  fs.writeFile('pedestrian-count-data.json', JSON.stringify(pedestrianGeoJson), 'utf8', () =>
    console.log('--> completed migration')
  )
}

export const mapParkingSensorData = () => {
  const parkingSensors = JSON.parse(JSON.stringify(require('./parking-sensor-data.json')))
  const geoJsonPropertiesObject = parkingSensors.reduce((geoJsonFeatures, sensor, index) => {
    const { geometry, ...rest } = sensor
    const polygon = Array.from(geometry)
    const feature = {
      type: 'Feature',
      properties: { ...rest },
      geometry: {
        type: 'MultiPolygon',
        coordinates: [[[...polygon]]]
      }
    }

    return [...geoJsonFeatures, feature]
  }, [])

  console.log('--> completed mapping features')
  const geoJsonObject = {
    type: 'FeatureCollection',
    features: geoJsonPropertiesObject
  }

  fs.writeFile('parking-sensors-count-data-test.geojson', JSON.stringify(geoJsonObject), 'utf8', () =>
    console.log('--> completed migration')
  )
}
