// @flow

import React, { Component } from 'react'
import { withSiteData } from 'react-static'
import styled from 'styled-components'
import { Card, Col, Row } from 'antd'
import Page from 'components/Layout/Page'

const BannerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 200px;
  background-image: linear-gradient(109.6deg, rgba(231, 120, 120, 1) 11.2%, rgba(246, 220, 111, 1) 91.2%);
  justify-content: center;
  align-items: center;
  background-color: #ffca00;
  text-align: center;
`

class ViewProfile extends Component<Props, State> {
  state = {}

  componentDidMount = () => {}

  render() {
    return (
      <div>
        <BannerWrapper>
          <h1>
            Coming soon...
            <span aria-label="emoji" role="img">
              ❤️
            </span>
          </h1>
        </BannerWrapper>
        <Page>
          <Row>
            <Col span={12}>
              <Card style={{ marginBottom: '20px', marginRight: '5px' }}>
                <h1>Story</h1>
                <p>
                  Buskers have been part of cityscapes for centuries with many governments embracing them as a cultural
                  asset. They play a role in making our city more vibrant and lively and are strongly supported by the
                  community.
                </p>
                <p>However, busking isn't always as easy when there are: </p>
                <p>1. Strict regulations (i.e. 2 hour time slots)</p>
                <p>2. People carrying less and less cash </p>
                <p>3. Lack of resources and technology available.</p>
                <p>
                  For GovHack 2018, the Shooting Unicorns team created 'The Next Busker', a platform using open data for
                  buskers to identify the best time and location to perform. The platform analyses data using pedestrian
                  volume to determine hotspots for getting the best audience reach.
                </p>
                <p>
                  To help buskers identify the best time and location to perform, we aggregated pedestrian traffic to
                  determine the average count for each sensor by week day and hour. To use this data in a meaningful way
                  while factoring in good user experience, we built a map allowing buskers to select a time and date
                  that fits their availability.
                  <p>
                    Using the City Of Melbourne's Open Data SOCRATA API, we then filtered sensor locations based on the
                    radius of the selected landmark of interest and displayed hotspot areas for buskers to choose. >
                  </p>
                </p>
                <p>
                  We also wanted to experiment with the idea of re-purposing under utilised parking bays for busking (an
                  'alpha' feature since it hasn't been approved by the council yet). To achieve this, the platform uses
                  on-street parking bay sensors in conjunction with on street parking sensor data 2017 and on street
                  parking bays data to calculate the aggregated daily usage of parking bays across the City of
                  Melbourne. This allows buskers to also see on-peak and off-peak parking bays on the platform based on
                  their selected time and location.
                </p>
                <p>
                  Our mission is to create a platform for buskers to be discovered by Melbournians and tourists,
                  schedule their performance time to notify followers and the ability to get paid online. For the
                  community, this is a platform that brings people together to discover, subscribe and support their
                  favourite performers.
                </p>
                <br />
              </Card>
            </Col>
            <Col span={12}>
              <Card style={{ marginBottom: '20px', marginLeft: '5px' }}>
                <h1>Team</h1>
                <img
                  style={{ width: '100%', marginBottom: '20px' }}
                  src={require('images/shooting-unicorns-team.png')}
                  alt="Shooting Unicorns team"
                />
                <p>
                  Luannie (left) and Sam (right) are both Software Engineers and partners in crime who enjoys building
                  all things tech. Their bread and butter is React and Node JS!
                </p>
              </Card>
            </Col>
          </Row>
          <Card style={{ marginBottom: '20px' }}>
            <h4>Datasets</h4>
            <p>On-street Parking Bays</p>
            <p>On-street Car Parking Sensor Data - 2017</p>
            <p>Pedestrian volume (updated monthly)</p>
            <p>Pedestrian sensor locations</p>
            <p>On-street Parking Bay Sensors</p>
          </Card>
          <Card style={{ marginBottom: '20px' }}>
            <h4>Credit</h4>
            <p>Stock photos: https://unsplash.com/</p>
            <p>Stock photos: https://www.pexels.com/</p>
          </Card>
        </Page>
      </div>
    )
  }
}

export default withSiteData(ViewProfile)
