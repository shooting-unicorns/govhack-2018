// @flow

import React, { Component } from 'react'
import { Link, withSiteData } from 'react-static'
import styled from 'styled-components'
import _get from 'lodash/get'
import queryString from 'query-string'
import { List, Card, Avatar } from 'antd'
import Page from 'components/Layout/Page'
import { MOCK_PERFORMANCES } from 'mockData'

const BannerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 200px;
  background-image: linear-gradient(109.6deg, rgba(231, 120, 120, 1) 11.2%, rgba(246, 220, 111, 1) 91.2%);
  justify-content: center;
  align-items: center;
  background-color: #ffca00;
  text-align: center;
`

const getIsNewFromQueryParams = (location: Object) => _get(queryString.parse(location.search), 'isNew', false)

type Props = {
  location: Object,
  mockPerformances: Array<Object>
}

type State = {
  isNew: boolean,
  mockPerformances: null
}

class ViewMain extends Component<Props, State> {
  state = {
    isNew: getIsNewFromQueryParams(this.props.location)
  }

  componentDidMount = () => {
    const { location } = this.props
    const { isNew } = this.state

    if (isNew) {
      const landmark = _get(queryString.parse(location.search), 'location')
      const time = _get(queryString.parse(location.search), 'time')
      const date = _get(queryString.parse(location.search), 'date')

      const newPerformance = {
        id: '9',
        artistId: '2',
        artist: 'Shooting Unicorns',
        location: landmark,
        upcoming: date,
        time: time > 11 ? `${time}:00 PM` : `${time}:00 AM`
      }
      this.setState({ mockPerformances: [newPerformance, ...MOCK_PERFORMANCES] })
    } else {
      this.setState({ mockPerformances: MOCK_PERFORMANCES })
    }
  }

  render() {
    const { Meta } = Card
    const { mockPerformances } = this.state
    return (
      <div>
        <BannerWrapper>
          <h1>
            Discover the Buskers of Melbourne{'  '}
            <span aria-label="emoji" role="img">
              ❤️
            </span>
          </h1>
          <Link to="/about">Learn more</Link>
        </BannerWrapper>
        <Page>
          <div>
            <div>
              <List
                grid={{ gutter: 14, column: 4 }}
                dataSource={mockPerformances}
                renderItem={item => (
                  <List.Item>
                    <Link to={`/profile/view?id=${item.artistId}`}>
                      <Card
                        style={{ width: 300, marginBottom: '20px' }}
                        cover={<img alt="example" src={require(`images/${item.artistId}.png`)} />}
                      >
                        <Meta
                          avatar={<Avatar src={require(`images/${item.artistId}.png`)} />}
                          title={item.artist}
                          description={this.renderDescription(item)}
                        />
                      </Card>
                    </Link>
                  </List.Item>
                )}
              />
            </div>
          </div>
        </Page>
      </div>
    )
  }

  renderDescription = item => (
    <div>
      {item.upcoming} <br /> {item.time}
    </div>
  )
}

export default withSiteData(ViewMain)
