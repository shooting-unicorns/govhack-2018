// @flow

import React, { Component } from 'react'
import { withSiteData } from 'react-static'
import _get from 'lodash/get'
import _find from 'lodash/find'
import _filter from 'lodash/filter'
import styled from 'styled-components'
import YouTube from 'react-youtube'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram, faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons'
import queryString from 'query-string'
import { Card, Icon, Button } from 'antd'
import Page from 'components/Layout/Page'
import { MOCK_ARTISTS, MOCK_PERFORMANCES } from 'mockData'

const Banner = styled.div`
  display: flex;
  flex-direction: column;
  height: 200px;
  background-color #bd4f6c;
  background-image linear-gradient(326deg, #bd4f6c 0%, #d7816a 74%);
  justify-content: center;
  align-items: center;
  text-align: center;
`

const ProfilePhoto = styled.img`
  height: 150px;
  width: 150px;
  border-radius: 50%;
  object-fit: cover;
  position: absolute;
  left: 5%;
  top: 20%;
`

const SocialMediaIcon = styled.a`
  padding-right: 12px;
  color: white;
`

const opts = {
  height: '390',
  width: '640',
  playerVars: {
    autoplay: 1
  }
}

type Props = {
  location: string
}

type State = {
  profile: Object,
  performances: Array<Object>
}

class ViewProfile extends Component<Props, State> {
  state = {
    profile: null,
    performances: null
  }

  componentDidMount = () => {
    const { location } = this.props
    const profileId = _get(queryString.parse(location.search), 'id')
    const profile = _find(MOCK_ARTISTS, { id: profileId })
    const performances = _filter(MOCK_PERFORMANCES, { artistId: profileId })
    this.setState({ profile, performances })
  }

  handleOnReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo()
  }

  render() {
    const { profile, performances } = this.state
    return (
      <div>
        <Banner>
          {profile && <ProfilePhoto src={require(`images/${profile.id}.png`)} />}
          <h1>
            <strong>{_get(profile, 'artist')}</strong>
          </h1>
          <div>
            <SocialMediaIcon href="https://instagram.com/shooting.unicorns" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faInstagram} />
            </SocialMediaIcon>
            <SocialMediaIcon href="https://twitter.com/es_unicorns" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faTwitter} />
            </SocialMediaIcon>
            <SocialMediaIcon
              href="https://www.facebook.com/shooting.unicorns"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FontAwesomeIcon icon={faFacebook} />
            </SocialMediaIcon>
          </div>
        </Banner>

        <Page>
          <div style={{ display: 'flex', width: '100%', justifyContent: 'space-around', alignItems: 'center' }}>
            <div>
              <h4>NEXT PERFORMANCES</h4>
              {performances && <div>{performances.map((item) => this.renderPerformances(item))}</div>}
            </div>
            <div style={{ textAlign: 'center' }}>
              <YouTube videoId={_get(profile, 'video')} opts={opts} onReady={this.handleOnReady} />
              <p>Disclaimer: this is a random video from YouTube</p>
              <p style={{ width: '640px', marginBottom: '50px' }}>
                Thanks so much for all your support! I am extremely fortunate to have viewers like you, and can say with
                full confidence that busking would not have been possible without you.
              </p>
              <a target="_blank" rel="noopener noreferrer" href="https://www.patreon.com/shootingunicorns/creators">
                <Button type="primary">Support {_get(profile, 'artist')}</Button>
              </a>
            </div>
          </div>
        </Page>
      </div>
    )
  }

  renderPerformances = (item) => (
    <Card key={item.id} style={{ width: '300px', marginBottom: '20px' }}>
      <Icon type="info-circle" theme="twoTone" style={{ marginRight: '5px' }} />
      {item.upcoming} <br /> <Icon type="clock-circle" theme="twoTone" style={{ marginRight: '5px' }} />
      {item.time} <br /> <Icon type="compass" theme="twoTone" style={{ marginRight: '5px' }} />
      {item.location}
    </Card>
  )
}

export default withSiteData(ViewProfile)
