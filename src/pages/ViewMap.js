// @flow

import React from 'react'
import { Link } from 'react-static'
import moment from 'moment'
import styled from 'styled-components'
import find from 'lodash/fp/find'
import map from 'lodash/fp/map'
import _get from 'lodash/get'
import _isEmpty from 'lodash/isEmpty'
import { Card, Calendar, Slider, Button, Input, Modal } from 'antd'

// SDK
import { getNearBySensors } from 'sdk/pedestrian-sensors'

// Components
import DropdownButton from 'components/DropdownButton'
import ErrorBoundary from 'components/ErrorBoundary'
import Map from 'components/Map'

// Data
import { LANDMARKS } from 'data/landmarks'

type Props = {}

type State = {
  selectedLandmark: string,
  selectedHour: number,
  selectedDay: string,
  isVisible: boolean
}

const MapLegend = styled.div`
  background: linear-gradient(to right, #2dc4b2, #3bb3c3, #669ec4, #8b88b6, #a2719b, #aa5e79);
  height: 12px;
  width: 100%;
`

const LegendHeader = styled.div`
  font-weight: bold;
  margin-top: 15px;
  margin-bottom: 5px;
`

const LegendLabel = styled.div``

const Row = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`
class PlanBusk extends React.Component<Props, State> {
  state = {
    selectedLandmark: {},
    selectedHour: 0,
    selectedDay: moment().format('dddd'),
    selectedDayFormatted: moment().format('dddd, MMMM DD'),
    sensors: [],
    isVisible: true
  }

  getNearbyInformation = async () => {
    const { selectedLandmark } = this.state
    const { location } = selectedLandmark
    if (_isEmpty(selectedLandmark)) {
      return []
    }
    const query = {
      $where: `within_circle(location,${location.lat},${location.long},250)`
    }
    const sensors = await getNearBySensors(query)
    const sensorIds = map('sensorid', sensors)

    this.setState({ sensors: sensorIds })
  }

  handleDateSelect = date => {
    this.setState({ selectedDay: date.format('dddd'), selectedDayFormatted: date.format('dddd, MMMM DD') })
  }

  handleUpdateTip = value => {
    const formattedTime = value > 11 ? `${value}:00 PM` : `${value}:00 AM`
    return formattedTime
  }

  handleTimeChange = (selectedHour: number) => {
    this.setState({ selectedHour }, this.getNearbyInformation)
  }

  handleLandmarkSelect = ({ key: landmarkId }) => {
    const selectedLandmark = find({ id: landmarkId }, LANDMARKS)
    this.setState({ selectedLandmark }, this.getNearbyInformation)
  }

  handleCloseModal = e => {
    this.setState({
      isVisible: false
    })
  }

  render() {
    const { selectedLandmark, selectedDay, selectedDayFormatted, selectedHour, sensors, isVisible } = this.state
    const selectedLandmarkName = _get(selectedLandmark, 'name', '')
    const filters = {
      sensors,
      hour: selectedHour,
      day: selectedDay
    }

    return (
      <React.Fragment>
        <ErrorBoundary>
          <div style={{ display: 'flex' }}>
            <Modal
              title="Login Success!"
              visible={isVisible}
              onOk={this.handleCloseModal}
              onCancel={this.handleCloseModal}
            >
              <h4>You're logged in as Shooting Unicorns 🦄</h4>
            </Modal>
            <Card>
              <div style={{ marginLeft: '5px', marginBottom: '20px' }}>
                <strong>Select time:</strong>
                <Slider dots minx={0} max={23} tipFormatter={this.handleUpdateTip} onChange={this.handleTimeChange} />
              </div>
              <div style={{ marginBottom: '20px' }}>
                <strong>Select Landmark:</strong>
                <DropdownButton
                  data={LANDMARKS}
                  value={selectedLandmarkName}
                  defaultValue="E.g. Melbourne Central"
                  onSelectItem={this.handleLandmarkSelect}
                />
              </div>
              <strong>Select Date:</strong>
              <Calendar
                fullscreen={false}
                style={{ width: 300, borderRadius: 4 }}
                onPanelChange={this.handleDateChange}
                onSelect={this.handleDateSelect}
              />
              <div>
                <LegendHeader>Pedestrian Traffic</LegendHeader>
                <MapLegend />
                <Row>
                  <LegendLabel>0</LegendLabel>
                  <LegendLabel>100</LegendLabel>
                  <LegendLabel>500</LegendLabel>
                  <LegendLabel>1000</LegendLabel>
                  <LegendLabel>1500</LegendLabel>
                  <LegendLabel>2000+</LegendLabel>
                </Row>
                <div>
                  <LegendHeader>Performance location:</LegendHeader>
                  <Input placeholder="Where will you perform?" />
                </div>
                <Row style={{ marginTop: '20px' }}>
                  <Link
                    to={`/?isNew=true&location=${selectedLandmarkName}&time=${selectedHour}&date=${selectedDayFormatted}`}
                  >
                    <Button type="primary">Schedule performance</Button>
                  </Link>
                </Row>
              </div>
            </Card>
            <div style={{ height: '100vh', width: '100%' }}>
              <Map filters={{ ...filters, sensors }} landmark={selectedLandmark} />
            </div>
          </div>
        </ErrorBoundary>
      </React.Fragment>
    )
  }
}

export default PlanBusk
