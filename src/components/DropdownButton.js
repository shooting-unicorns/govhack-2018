// @flow
import React from 'react'
import _isEmpty from 'lodash/isEmpty'
import { Menu, Dropdown, Button, Icon } from 'antd'

type DropdownMenuItem = {
  data: {
    id: string,
    name: string
  },
  onSelectItem: (...args: any) => void
}

type Props = {
  data: Array<DropdownMenuItem>,
  onSelectItem: (...arg: any) => void,
  name: string,
  value?: string,
  defaultValue?: string
}

const DropdownMenu = ({ data, onSelectItem }: DropdownMenuItem) => (
  <Menu onClick={onSelectItem}>{data.map(item => <Menu.Item key={`${item.id}`}>{item.name}</Menu.Item>)}</Menu>
)

const DropdownButton = ({ data, name, onSelectItem, value, defaultValue }: Props) => (
  <Dropdown overlay={<DropdownMenu data={data} trigger={['click']} onSelectItem={onSelectItem} />}>
    <Button style={{ marginLeft: 8 }}>
      {_isEmpty(value) ? defaultValue : value} <Icon type="down" />
    </Button>
  </Dropdown>
)

DropdownButton.defaultProps = {
  value: '',
  defaultValue: ''
}

export default DropdownButton
