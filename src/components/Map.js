import React from 'react'
import mapboxgl from 'mapbox-gl'
import _get from 'lodash/get'
import _map from 'lodash/fp/map'
import _isEmpty from 'lodash/isEmpty'
import moment from 'moment'

// Data
import pedestrianCountData from 'data/pedestrian-count-data.json'
import parkingBays from 'data/parking-sensors-count-data-test.geojson'
import { MB_PUBLIC_ACCESS_TOKEN, MAP_BOX_STYLE, MELBOURNE_GEOMETRY } from 'constants/mapbox'
import { LANDMARKS } from 'data/landmarks'

// Mapbox related imports
require('../stylesheets/mapbox.css')
require('../scripts/mapbox-gl')

// Initialise mapbox with API Token
mapboxgl.accessToken = MB_PUBLIC_ACCESS_TOKEN

type Props = {
  filters: {
    sensors: Array<string>,
    hour: number,
    day: string
  },
  landmark: {}
}

type State = {}

class Map extends React.Component<Props, State> {
  buildMap = () => {
    const { latitude, longitude } = MELBOURNE_GEOMETRY

    const mapConfig = {
      container: this.mapContainer,
      style: MAP_BOX_STYLE,
      center: [longitude, latitude],
      zoom: 15
    }

    const map = (this.map = new mapboxgl.Map(mapConfig))

    this.map.on('load', () => {
      // Add pedestrian count information
      this.map.addLayer({
        id: 'pedestrian-count',
        type: 'circle',
        source: {
          type: 'geojson',
          data: pedestrianCountData
        },
        paint: {
          'circle-radius': {
            property: 'median_count',
            type: 'exponential',
            stops: [[10, 5], [100, 8], [500, 12], [1000, 13], [1500, 15], [2000, 18], [3000, 20]]
          },
          'circle-color': {
            property: 'median_count',
            stops: [
              [0, '#2dc4b2'],
              [100, '#3bb3c3'],
              [500, '#669ec4'],
              [1000, '#8b88b6'],
              [1500, '#a2719b'],
              [2000, '#aa5e79'],
              [3000, '#ff0000']
            ]
          }
        }
      })

      this.map.addLayer({
        id: 'parking-bays',
        type: 'fill',
        source: {
          type: 'geojson',
          data: parkingBays
        },
        layout: {},
        paint: {
          'fill-color': {
            property: 'Count of Number of Records',
            stops: [
              [0, '#FFCC80'],
              [100, '#FFA726'],
              [500, '#FB8C00'],
              [1000, '#E65100'],
              [1500, '#EF5350'],
              [2000, '#F44336'],
              [3000, '#B71C1C']
            ]
          },
          'fill-outline-color': '#088',
          'fill-opacity': 0.7
        }
      })

      // Add landmark marker onto map
      _map(landmark => {
        const { name, description } = landmark
        const popup = new mapboxgl.Popup().setHTML(
          `<div style="width: 150px;"><h3>${name}</h3><p>${description}</p></div>`
        )
        const long = _get(landmark, 'location.long', 0)
        const lat = _get(landmark, 'location.lat', 0)
        return new mapboxgl.Marker()
          .setLngLat([long, lat])
          .setPopup(popup)
          .addTo(map)
      })(LANDMARKS)
      this.updateMap(this.map)
    })
  }

  componentDidMount() {
    this.buildMap()
  }

  componentWillUnmount() {
    if (this.map) {
      this.map.remove()
    }
  }

  componentDidUpdate() {
    if (this.map) {
      this.updateMap(this.map)
    }
  }

  updateMap = map => {
    const { filters } = this.props

    // Set filter based on hour, day and filter the sensors
    const hour = _get(filters, 'hour', 0)
    const day = _get(filters, 'day', moment().format('dddd'))
    const sensors = _get(filters, 'sensors', [])

    const sensorIds = sensors.map(sensor => parseInt(sensor, 10))
    const dayFilter = ['==', 'day', day]
    const hourFilter = ['==', 'hour', hour]
    const sensorFilter = ['in', 'sensorid', ...sensorIds]

    if (!_isEmpty(sensors)) {
      map.setFilter('pedestrian-count', ['all', dayFilter, hourFilter, sensorFilter])
    }

    map.setFilter('parking-bays', ['==', 'day', day])
  }

  render() {
    return (
      <React.Fragment>
        <div ref={el => (this.mapContainer = el)} id="map-container" style={{ height: '100%' }} />
      </React.Fragment>
    )
  }
}

export default Map
