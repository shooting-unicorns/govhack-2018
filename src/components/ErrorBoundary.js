// @flow
import React, { type Node } from 'react'

type Props = {
  children: Node
}

type State = {
  hasErrored: boolean,
  errorMessage: string
}

class ErrorBoundary extends React.Component<Props, State> {
  state = {
    hasErrored: false
  }

  componentDidCatch(error, info) {
    this.setState({
      hasErrored: true,
      errorMessage: error.message
    })
  }

  render() {
    const { children } = this.props
    const { hasErrored, errorMessage } = this.state

    return (
      <React.Fragment>
        {hasErrored ? (
          <div>
            <div> An error occured whilst rendering </div>
            <div> {errorMessage} </div>
          </div>
        ) : (
          <React.Fragment> {children} </React.Fragment>
        )}
      </React.Fragment>
    )
  }
}

export default ErrorBoundary
