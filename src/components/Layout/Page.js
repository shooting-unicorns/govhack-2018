// @flow
import React from 'react'
import styled from 'styled-components'

const Content = styled.div`
  padding: 5rem;
  z-index: 1;

  @media screen and (max-width: 600px) {
    margin-left: -1.5rem;
    margin-right: -1.5rem;
  }
`

type Props = {
  children: Node
}
const Page = ({ children }: Props) => <Content>{children}</Content>

export default Page
