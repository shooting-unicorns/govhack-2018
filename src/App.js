// @flow

import React from 'react'
import { Link, Router } from 'react-static'
import styled, { injectGlobal } from 'styled-components'
import { hot } from 'react-hot-loader'
import Routes from 'react-static-routes'
import { Layout, Button } from 'antd'
import ErrorBoundary from 'components/ErrorBoundary'

injectGlobal`
  #root {
    min-width: 100%;
    min-height: 100%;
    display: flex;
  }
`

const SigninWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
`

const TitleWrapper = styled.div`
  display: 'flex'
  justfy-content: center;
  text-align: center;
  position: absolute;
  top: 0px;
  left: 50%
  transform: translate(-50%);
`

class App extends React.Component<Props, State> {
  render() {
    const { Header, Content, Footer } = Layout
    return (
      <Router>
        <Layout>
          <Header
            style={{
              color: 'white',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              backgroundColor: '#273643'
            }}
          >
            <SigninWrapper>
              <TitleWrapper>
                <Link to="/">
                  <h1 style={{ color: 'white' }}>
                    <strong>THE NEXT BUSKER</strong>
                  </h1>
                </Link>
              </TitleWrapper>
              Are you a busker?
              <Link to="/plan">
                <Button type="primary" style={{ marginLeft: '20px' }}>
                  Sign in
                </Button>
              </Link>
            </SigninWrapper>
          </Header>
          <ErrorBoundary>
            <Content>
              <Routes />
            </Content>
          </ErrorBoundary>
          <Footer style={{ background: '#fafafa', textAlign: 'center' }}>
            © <a href="https://shooting-unicorns.com"> Shooting Unicorns </a> | ABN 97 245 651 731
          </Footer>
        </Layout>
      </Router>
    )
  }
}

export default hot(module)(App)
