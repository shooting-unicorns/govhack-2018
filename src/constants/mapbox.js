export const MAP_BOX_API_KEY =
  'sk.eyJ1IjoiZXMtdW5pY29ybnMiLCJhIjoiY2psc3RnbjVrMDBxYTNwbzZmaWo4dGxobSJ9.heFm_uasNEn0e1ACaM5Rwg'
export const MB_PUBLIC_ACCESS_TOKEN =
  'pk.eyJ1IjoiZXMtdW5pY29ybnMiLCJhIjoiY2psc25hbTB0MDQ0cDNwcDQ1bHNhZm02OCJ9.ndt6-CZY34BzJGkU-5TXog'
export const MAP_BOX_STYLE = 'mapbox://styles/mapbox/light-v9'
export const MELBOURNE_GEOMETRY = {
  latitude: -37.8137894,
  longitude: 144.9624562
}
