// @flow

import request from 'sdk/requests'

export const getExamplePosts = async () => request({ url: '/posts', method: 'GET' })

export const getExamplePostsWithQueries = async (query: { userId: number }) =>
  request({ url: '/posts', method: 'GET', params: query })
