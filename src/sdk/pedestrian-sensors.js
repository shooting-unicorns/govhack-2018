import request from 'sdk/requests'

// https://data.seattle.gov/resource/pu5n-trf4.json?$where=within_circle(incident_location, 47.59815, -122.334540, 500)

export const getNearBySensors = async (query: Object) =>
  request({ url: `resource/xbm5-bb4n.json`, method: 'GET', params: query })


