export const MOCK_ARTISTS = [
  {
    id: '1',
    artist: 'Acoustic seasons',
    video: 'BztU8Als7ZU'
  },
  {
    id: '2',
    artist: 'Shooting Unicorns',
    video: 'yMq9gpgcRNM'
  },
  {
    id: '3',
    artist: 'Not some tofu',
    video: 'oRxGLCExGQE'
  },
  {
    id: '4',
    artist: 'Annie Lu',
    video: '2ONhQSMVaeg'
  }
]

export const MOCK_PERFORMANCES = [
  {
    id: '1',
    artistId: '1',
    artist: 'Acoustic seasons',
    location: 'State Library',
    upcoming: 'Friday, September 7',
    time: '6:00PM'
  },
  {
    id: '2',
    artistId: '4',
    artist: 'Annie Lu',
    location: 'Melbourne Central',
    upcoming: 'Saturday, September 8',
    time: '12:00PM'
  },
  {
    id: '3',
    artistId: '2',
    artist: 'Shooting Unicorns',
    location: 'Bourke St, Melbourne',
    upcoming: 'Sunday, September 9',
    time: '2:00PM'
  },
  {
    id: '4',
    artistId: '3',
    artist: 'Not some tofu',
    location: 'Bourke St, Melbourne',
    upcoming: 'Tuesday, September 11',
    time: '7:30PM',
    video: 'oRxGLCExGQE'
  },
  {
    id: '5',
    artistId: '4',
    artist: 'Annie Lu',
    location: 'Batman Park',
    upcoming: 'Thursday, September 13',
    time: '6:30PM',
    video: 'HXiHL9cGCmE'
  },
  {
    id: '6',
    artistId: '2',
    artist: 'Shooting Unicorns ',
    location: 'Flinders Street Station',
    upcoming: 'Saturday, September 15',
    time: '9:00AM',
    video: ''
  },
  {
    id: '7',
    artistId: '3',
    artist: 'Not some tofu',
    location: 'Federation Square',
    upcoming: 'Sunday, September 16',
    time: '1:00PM',
    video: ''
  },
  {
    id: '8',
    artistId: '2',
    artist: 'Shooting Unicorns',
    location: 'Bourke St, Melbourne',
    upcoming: 'Saturday, September 22',
    time: '4:00PM',
    video: ''
  }
]
